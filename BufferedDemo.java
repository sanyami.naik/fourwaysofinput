package practice.FileHandling;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class BufferedDemo {

    public static void main(String[] args) throws IOException {
        String name;
        int id;
        float marks;
        BufferedReader br=new BufferedReader(new InputStreamReader(System.in));

        System.out.println("Enter the name of Student");
        name=br.readLine();

        System.out.println("Enter the id of Student");
        id=Integer.parseInt(br.readLine());

        System.out.println("Enter the marks of Student");
        marks=Float.parseFloat(br.readLine());


        System.out.println("The name of student is "+name+" with id "+id+" has scored "+marks);


    }
}


/*
Output:
Enter the name of Student
Sanyami
Enter the id of Student
45
Enter the marks of Student
56
The name of student is Sanyami with id 45 has scored 56.0

 */