package practice.FileHandling;


import java.util.Scanner;

public class ScannerDemo {
    public static void main(String[] args) {


        String name,city;
        int id;
        float marks;
        Scanner sc = new Scanner(System.in);

        System.out.println("Enter the id of Student");
        System.out.println("Enter the name of Student");
        System.out.println("Enter the city of Student");
        System.out.println("Enter the marks of Student");

        id = sc.nextInt();
        sc.nextLine();
        name = sc.nextLine();
        city=sc.nextLine();
        marks=sc.nextFloat();


        System.out.println("The name of student is " + name + " with id " + id + " has scored " + marks+" is from "+city);
    }
}



/*
OUTPUT
Enter the id of Student
Enter the name of Student
Enter the city of Student
Enter the marks of Student
45
Sanyami
Mahad
78
The name of student is Sanyami with id 45 has scored 78.0 is from Mahad

 */