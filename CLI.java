package practice.FileHandling;

public class CLI {
    public static void main(String[] args)
    {
        int sum=0;
        for(String s:args)
        {
            int i=Integer.parseInt(s);
            System.out.print(i+ " ");
            sum=sum+i;
        }

        System.out.println();
        System.out.println("the sum of the above numbers is");
        System.out.println(sum);
    }
}

/*
OUTPUT
1 2 3 4 5 6 7 8
the sum of the above numbers is
36
 */